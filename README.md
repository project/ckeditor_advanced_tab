CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * FAQ
 * Maintainers


INTRODUCTION
------------

This module integrates the
[dialogadvtab](https://ckeditor.com/cke4/addon/dialogadvtab) CKEditor plugin
with Drupal.

This plugin provides the Advanced dialog window tab to extend some editor dialog
windows. Other plugins don't need to implement the same features for their
dialog windows.

Some of the CKEditor plugins that use the Advanced tab are:

 * The [Link](https://ckeditor.com/cke4/addon/link) plugin
 * The [Image](https://ckeditor.com/cke4/addon/image) plugin
 * The [Flash](https://ckeditor.com/cke4/addon/flash) plugin
 * The [Table](https://ckeditor.com/cke4/addon/table) plugin
 * The [IFrame](https://ckeditor.com/cke4/addon/iframe) plugin
 * The [Div](https://ckeditor.com/cke4/addon/div) Container plugin


REQUIREMENTS
------------

This module requires the following modules:

* [CKEditor](https://www.drupal.org/docs/8/core/modules/ckeditor)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

 * Run `composer require --prefer-dist composer/installers` to ensure you have
   the composer/installers package. This facilitates installation into
   directories other than vendor using Composer.

 * Make sure the "extras/installer-paths" section in the composer.json file has
   an entry for `type:drupal-library`, for example the following one.

   ```json
   {
       "libraries/{$name}": ["type:drupal-library"]
   }
   ```

 * Add the following lines to the "repositories" section in the composer.json
   file. The 4.18.0 release of the plugin is the only release that is defined
   compatible with the release of CKEditor used by Drupal 9.x.

   ```json
   {
       "type": "package",
       "package": {
           "name": "ckeditor/dialogadvtab",
           "version": "4.18.0",
           "type": "drupal-library",
           "extra": {
               "installer-name": "ckeditor/plugins/dialogadvtab"
           },
           "dist": {
               "url": "https://download.ckeditor.com/dialogadvtab/releases/dialogadvtab_4.18.0.zip",
               "type": "zip"
           }
       }
   }
   ```

 * Run `composer require 'ckeditor/dialogadvtab:4.18.0'` to download the plugin.


CONFIGURATION
-------------

 * Configure the WYSIWYG buttons in
  Administration » Configuration » Content authoring » Text formats and editors

   - Drag the advanced icon button to your toolbar and save


FAQ
---

Q: Can I use a release of the plugin that is more recent than the one indicated
   by the README.md file?

A: The README file shows how to install the plugin its
   [project page](https://ckeditor.com/cke4/addon/dialogadvtab) defines
   compatible with the release of CKEditor used by the Drupal release required
   by the module. You can install a more recent version, if you find out it's
   compatible with the Drupal release you are using.
   The README.md file will be updated to show how to install the plugin
   compatible with the Drupal release required by the module.


MAINTAINERS
-----------

Current maintainers:
 * Alberto Paderno (apaderno) - https://www.drupal.org/u/apaderno
 * Chris Burge - https://www.drupal.org/u/chris-burge
 * Julien de Nas de Tourris (julien) - https://www.drupal.org/u/julien
 * Stephen Mulvihill (smulvih2) - https://www.drupal.org/u/smulvih2

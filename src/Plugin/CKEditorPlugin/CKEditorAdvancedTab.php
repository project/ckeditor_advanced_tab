<?php

declare(strict_types=1);

namespace Drupal\ckeditor_advanced_tab\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\editor\Entity\Editor;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "Advanced Tab" plugin.
 *
 * @CKEditorPlugin(
 *   id = "dialogadvtab",
 *   label = @Translation("Advanced Tab Dialog")
 * )
 */
class CKEditorAdvancedTab extends CKEditorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal root directory.
   *
   * @var string
   */
  protected $root;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new CKEditorAdvancedTab object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param string $root
   *   Drupal root directory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation_manager
   *   The translation manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, string $root, LoggerInterface $logger, TranslationInterface $translation_manager, ModuleExtensionList $extension_list_module) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->root = $root;
    $this->logger = $logger;
    $this->setStringTranslation($translation_manager);
    $this->moduleList = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->getParameter('app.root'),
      $container->get('logger.channel.ckeditor_advanced_tab'),
      $container->get('string_translation'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $file = 'libraries/ckeditor/plugins/dialogadvtab/plugin.js';
    if (!file_exists($this->root . '/' . $file)) {
      $this->logger->error(
        'The %file file cannot be found. Please follow the installation instructions given in the README.md file.',
        ['%file' => $file]
      );
    }
    else {
      return $file;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'AdvancedTab' => [
        'label' => $this->t('Advanced Tab'),
        'image' => $this->getModulePath('ckeditor_advanced_tab') . '/icons/link.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
